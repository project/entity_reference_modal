# Entity reference modal module

Module provides a generic entity reference suggestion with create new entity
with modal. It can be used in any context where one needs to select a few
entities & do something with them. Read more online documentation
<a href="https://www.drupal.org/project/entity_reference_modal">Entity reference
modal</a>

## How to use
  - General
  - Entity Reference Modal comes with an entity reference field widget
  - Embedding entities into ckeditor 5
### Example use cases
  - You need an auto-suggest entity, but you can't find it, so you need to
create a new entity without submitting your current form.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

- Configuration in field reference widget. (if your theme is not bootstrap
then you have to enable bootstrap option)
- Active button in ckeditor

## Maintainers

- [Bao NGUYEN](https://www.drupal.org/u/lazzyvn)
